

CryptoNotes_PA
==============
A progressive web app for encrypting notes.


Functionality
-------------

* **Encrypt** notes using `AES-256-CBC` with custom **passphrase** `(bcrypt (60b) -> SHA-256)`, and **auto-generated** `IV (128b)`
* **List** records by name in the `List` view with **actions**
* **Remove** records
* **Decrypt** notes and **view** the content in the `Detail` view
* The individual records can be **exported** as `JSON` file
* `JSON` file can be used to **import** a record


Installation & Running
----------------------

The app can be run using HTTP or HTTPS. Check out `srv/config.json`. HTTP will work for localhost. Self-signed certificate will not work in Chrome (will not allow to load ServiceWorker), however may work in Firefox.

1. Clone the repository
2. Generate ssl certificate & key  
`openssl req -x509 -newkey rsa:4096 -keyout srv/key.pem -out srv/cert.pem -days 7 -nodes`
3. Start server `npm start`
4. Access app `https://localhost:8000`


#### Unregister ServiceWorker

Using `about:serviceworkers` / `chrome://serviceworker-internals`, or programmatically via console:
```js
navigator.serviceWorker.getRegistrations().then(function(registrations) {
    for(let registration of registrations) {
        registration.unregister()
    }
})
```

#### Clear Cache
Programatically via console:
```js
caches.delete('v1'); // 'v1' cache name
```
or `Hard Reload and Clear Cache` .

#### Persistence
Note that `nedb` library used for persistence chooses the underlying storage technology (it could be IndexDB, WebSQL, LocalStorage).


To Be Implemented
--------------------
* Loading Screen & UI Locking.
