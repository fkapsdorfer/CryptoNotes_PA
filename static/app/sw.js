/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file ServiceWorker for offline functionality.
 */

var cacheName = 'v1';
var cacheFiles = [
    "./static/app/app.js",
    "./static/app/controllers/Detail.js",
    "./static/app/controllers/Dialog.js",
    "./static/app/controllers/Header.js",
    "./static/app/controllers/List.js",
    "./static/app/controllers/Lock.js",
    "./static/app/controllers/Unlock.js",
    "./static/app/directives/dialog.html",
    "./static/app/directives/dialog.js",
    "./static/app/directives/password.html",
    "./static/app/directives/password.js",
    "./static/app/directives/srImport.js",
    "./",
    "./static/app/services/Base64Lib.js",
    "./static/app/services/CryptoLib.js",
    "./static/app/services/SecureRecord.js",
    "./static/app/services/SecureRecordCollection.js",
    "./static/app/services/download.js",
    "./static/app/sw.js",
    "./static/app/templates/detail.html",
    "./static/app/templates/list.html",
    "./static/app/templates/lock.html",
    "./static/app/templates/unlock.html",
    "./static/assets/css/icomoon/fonts/icomoon.eot",
    "./static/assets/css/icomoon/fonts/icomoon.svg",
    "./static/assets/css/icomoon/fonts/icomoon.ttf",
    "./static/assets/css/icomoon/fonts/icomoon.woff",
    "./static/assets/css/icomoon/style.css",
    "./static/assets/css/main.css",
    "./static/assets/vendor/angular/angular.min.js",
    "./static/assets/vendor/angular-ui-router/angular-ui-router.min.js",
    "./static/assets/vendor/bcrypt/bcrypt.min.js",
    "./static/assets/vendor/jquery/jquery.min.js",
    "./static/assets/vendor/nedb/nedb.min.js"
];


self.addEventListener('install', (evt) => {
    console.log('Installing...');

    // Precache files
    evt.waitUntil(
        caches.open(cacheName).then((cache) => {
            console.log('Caching files...');
            return cache.addAll(cacheFiles);
        })
    );
});

self.addEventListener('activate', (evt) => {
    console.log('Activating...');

    // Remove old version cache
    evt.waitUntil(
        caches.keys().then((cacheNames) => {
            cacheNames.map((thisCacheName) => {
                if (thisCacheName !== cacheName) {
                    console.log(`Removing cache: ${thisCacheName} ...`);
                    return caches.remove(thisCacheName);
                }
            });
        })
    );
});

self.addEventListener('fetch', (evt) => {

    // Intercept requests and check if file/response not cached
    evt.respondWith(
        caches.match(evt.request).then((req) => {
            if (req) {
                console.log(`Found in chache: ${evt.request.url}`);
                return req;
            } else {
                console.log(`Fetching: ${evt.request.url}`);
                return fetch(evt.request);
            }
        })
    );
});
