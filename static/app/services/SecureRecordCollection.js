/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file A container for SecureRecords with persistence. Requires Nedb library (global scope).
 */

angular.module('CryptoNotes').factory('SecureRecordCollection', ['SecureRecord', '$rootScope', function (SecureRecord, $rootScope) {

    var Cls = function () {
        // Do not overwrite from outside
        this._collection = [];
        this._db = new Nedb({ filename: 'SecureRecordCollection', autoload: true });

        // Fill
        this._db.find({}, (err, docs) => {
            docs.forEach(doc => {
                this._collection.push(new SecureRecord(doc));
            });
            $rootScope.$broadcast('SRCollectionLoaded');
        });
    };

    /**
     * @prop {SecureRecord} sr
     * @return {Promise}
     */
    Cls.prototype.add = function (sr) {
        return new Promise((resolve) => {
            this._db.insert(sr.export(), (err, doc) => {
                sr._id = doc._id;
                this._collection.push(sr);
                $rootScope.$broadcast('SRCollectionUpdated');
                resolve();
            });
        });
    };

    /**
     * @prop {SecureRecord} sr
     * @return {Promise}
     */
    Cls.prototype.remove = function (sr) {
        return new Promise((resolve) => {
            this._db.remove({ _id: sr._id }, (err) => {
                this._collection.splice(this._collection.indexOf(sr), 1);
                $rootScope.$broadcast('SRCollectionUpdated');
                resolve();
            });
        });
    };

    return new Cls();

}]);
