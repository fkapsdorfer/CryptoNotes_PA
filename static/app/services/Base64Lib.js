/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file A library to encode and decode Base64 - ArrayBuffer.
 */

angular.module('CryptoNotes').factory('Base64Lib', [function () {

    var Base64 = {
        /**
         * @param  {ArrayBuffer} buffer  Input bytes
         * @return {String}              Base64 string
         */
        encode: function (buffer) {
            var base64 = '';
            var bytes  = new Uint8Array(buffer);
            var len    = bytes.byteLength;

            for (var i = 0; i < len; i++) {
                base64 += String.fromCharCode(bytes[i]);
            }

            return window.btoa(base64);
        },

        /**
         * @param  {String}      base64     Base64 input string
         * @return {ArrayBuffer}            Output bytes
         */
        decode: function (base64) {
            var str   = window.atob(base64);
            var len   = str.length;
            var bytes = new Uint8Array(len);

            for (var i = 0; i < len; i++) {
                bytes[i] = str.charCodeAt(i);
            }

            return bytes;
        }
    }

    return Base64;
}]);
