/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file A library to used for encryption.
 */

angular.module('CryptoNotes').factory('CryptoLib', ['Base64Lib', function (Base64) {

    var utfEncoder = new TextEncoder('utf-8');
    var utfDecoder = new TextDecoder('utf-8');


    /**
     * @param   {Uint8Array}            buff256     256 bit ArrayBuffer
     * @return  {Promise<CryptoKey>}
     */
    function createKey (buff256) {
        return crypto.subtle.importKey(
            "raw",
            buff256,
            {
                name: "AES-CBC"
            },
            true,
            [
                "encrypt",
                "decrypt"
            ]
        )
    }

    /**
     * @param   {CryptoKey}             key
     * @param   {Uint8Array}            iv      128 bit ArrayBuffer
     * @param   {Uint8Array}            data    Plaintext ArrayBuffer
     * @return  {Promise<ArrayBuffer>}          Resolves ciphertext
     */
    function encrypt (key, iv, data) {
        return crypto.subtle.encrypt({
            name : key.algorithm.name,
            iv   : iv
        }, key, data);
    }

    /**
     * @param   {CryptoKey}             key
     * @param   {Uint8Array}            iv      128 bit ArrayBuffer
     * @param   {Uint8Array}            data    Ciphertext ArrayBuffer
     * @return  {Promise<ArrayBuffer>}          Resolves plaintext
     */
    function decrypt (key, iv, data) {
        return crypto.subtle.decrypt({
            name : key.algorithm.name,
            iv   : iv
        }, key, data);
    }

    /**
     * @param   {Uint8Array}            input   ArrayBuffer
     * @return  {Promise<ArrayBuffer>}          256 bit ArrayBuffer
     */
    function sha256 (input) {
        return crypto.subtle.digest({ name: 'SHA-256' }, input);
    }



    var Crypto = {

        /**
         * @param   {String}                key
         * @param   {String}                data    Plaintext
         * @return  {Promise<Object>}               Resolves { String iv, String data }
         */
        encrypt: function (key, data) {
            return new Promise((resolve) => {
                var iv = crypto.getRandomValues(new Uint8Array(16));

                sha256(utfEncoder.encode(key))
                .then((shaHash) => {
                    return createKey(shaHash);
                }).then((key) => {
                    return encrypt(key, iv, utfEncoder.encode(data));
                }).then((enc) => {
                    resolve({
                        iv   : Base64.encode(iv),
                        data : Base64.encode(enc)
                    });
                });
            });
        },

        /**
         * Doesn't check if the key is right.
         *
         * @param   {String}                key
         * @param   {String}                iv      Initialization vector (128 bit)
         * @param   {String}                data    Ciphertext
         * @return  {Promise<String>}               Resolves plaintext
         */
        decrypt: function (key, iv, data) {
            return new Promise((resolve) => {
                sha256(utfEncoder.encode(key))
                .then((shaHash) => {
                    return createKey(shaHash);
                }).then((key) => {
                    return decrypt(key, Base64.decode(iv), Base64.decode(data));
                }).then((dec) => {
                    resolve(utfDecoder.decode(dec));
                });
            });
        },

        /**
         * @param   {String}                str
         * @param   {String}                hash    Bcrypt
         * @return  {Promise<Boolean>}              If matching
         */
        bcryptCompare: function (str, hash) {
            return new Promise((resolve, reject) => {
                dcodeIO.bcrypt.compare(str, hash, (err, res) => {
                    if (!err) {
                        resolve(res);
                    } else {
                        reject(err);
                    }
                });
            });
        },

        /**
         * @param   {String}                input
         * @return  {Promise<String>}               Bcrypt
         */
        bcrypt: function (input) {
            return new Promise((resolve, reject) => {
                dcodeIO.bcrypt.genSalt(10, (err, salt) => {
                    if (err) {
                        return reject(err);
                    }

                    dcodeIO.bcrypt.hash(input, salt, (err, hash) => {
                        if (!err) {
                            resolve(hash);
                        } else {
                            reject(err);
                        }
                    });
                });
            });
        }
    }

    return Crypto;
}]);
