
angular.module('CryptoNotes').factory('download', [function () {

    var obj = {};

    /**
     * Prompt downloading of a file with given data.
     *
     * @return {Boolean} if success
     */
    var download = function (data, fname, mime) {
    	var URL 	= window.URL;
    	var Blob 	= window.Blob;

    	if (!URL) {
    		URL = window.webkitURL;

    		if (!URL) {
    			return false;
    		}
    	}

    	if (!Blob) {
    		Blob = window.WebKitBlob;

    		if (!Blob) {
    			return false;
    		}
    	}


    	var blob = new Blob([data], { type: mime });
    	var url  = URL.createObjectURL(blob);

    	// simulate a button click to "force" downloading
    	var el         = document.createElement('a');
    	el.href        = url;
    	el.download    = fname;
    	document.body.appendChild(el);
    	el.click();
    	el.remove();

    	setTimeout(function(){
    		// free the memory
    		URL.revokeObjectURL(url);
    	}, 100);

    	return true;
    }

    return download;
}]);
