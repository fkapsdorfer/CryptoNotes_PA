

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js')
        .then((reg) => {
            console.log('serviceWorker.register() resolved');
            console.log(reg);
        })
        .catch((err) => {
            console.log('serviceWorker.register() rejected');
            console.log(err);
        });
}


angular.module('CryptoNotes', [
    'ui.router'
]).config([ '$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise("/list");

    $stateProvider.state('List', {
        url: '/list',
        controller: 'List',
        templateUrl: '/static/app/templates/list.html'
    }).state('Unlock', {
        url: '/unlock',
        controller: 'Unlock',
        templateUrl: '/static/app/templates/unlock.html',
        params: { sr: null }
    }).state('Detail', {
        url: '/detail',
        controller: 'Detail',
        templateUrl: '/static/app/templates/detail.html',
        params: { sr: null, data: null }
    }).state('Lock', {
        url: '/lock',
        controller: 'Lock',
        templateUrl: '/static/app/templates/lock.html',
        params: { sr: null, data: null }
    });
}]);
