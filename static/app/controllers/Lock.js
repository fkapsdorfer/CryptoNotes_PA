/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file The Lock state controller.
 */

angular.module('CryptoNotes').controller('Lock', ['$scope', '$state', 'SecureRecord', 'SecureRecordCollection', function($scope, $state, SecureRecord, src){

    $scope.sr = {
        // name, plainKey, plainData
    };

    $scope.actions = {
        submit: function () {
            var sr = new SecureRecord($scope.sr, () => {
                src.add(sr).then(() => {
                    $state.go('List');
                });
            });
        }
    }


}]);
