/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file Wrapper controller for dialog directive. Listens for "dialog" event (with parameters) on $rootScope and opens dialog.
 */

angular.module('CryptoNotes').controller('Dialog', ['$scope', function($scope){

    $scope.dialog = {
        title: 'Default Title',
        msg: 'Default message.',
        ctrl: {
            // onOpen(), onClose()
            // defined by directive: open(), hide(), shown
        }
    };

    $scope.$root.$on('dialog', (evt, o) => {

        $scope.$apply(() => {
            $scope.dialog.title = o.title;
            $scope.dialog.msg   = o.msg;

            if (o.ctrl && typeof o.ctrl.onOpen === 'function') {
                $scope.dialog.onOpen = o.onOpen;
            }
            if (o.ctrl && typeof o.ctrl.onClose === 'function') {
                $scope.dialog.onClose = o.onClose;
            }
        });

        $scope.dialog.ctrl.open();
    });

}]);
