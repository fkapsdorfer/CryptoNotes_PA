/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file The Unlock state controller.
 */

angular.module('CryptoNotes').controller('Unlock', ['$scope', '$state', '$stateParams', 'SecureRecord', 'SecureRecordCollection', function($scope, $state, $stateParams, SecureRecord, src){
    var sr = $scope.sr = $stateParams.sr;

    if (!sr) {
        $state.go('List');
    }

    $scope.actions = {
        submit: function () {
            $scope.sr.getData($scope.unlock.key)
            .then(data => {
                $state.go('Detail', { sr: sr, data: data });
            })
            .catch(err => {
                $scope.$root.$broadcast('dialog', {
                    title: 'Bad Key',
                    msg: 'You must have entered bad key.'
                });
            });
        }
    }


}]);
