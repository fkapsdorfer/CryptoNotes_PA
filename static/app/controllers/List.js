/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file The List state controller.
 */

angular.module('CryptoNotes').controller('List', ['$scope', '$state', 'SecureRecord', 'SecureRecordCollection', 'download', function($scope, $state, SecureRecord, src, download){
    $scope.src = src._collection;


    $scope.$on('SRCollectionLoaded', () => {
        $scope.$digest();
    });

    $scope.$root.$on('SRCollectionUpdated', () => {
        $scope.$digest();
    });


    $scope.actions = {
        removeSr: function (sr) {
            src.remove(sr);
        },
        unlockSr: function (sr) {
            $state.go('Unlock', { sr: sr });
        },
        exportSr: function (sr) {
            var name = sr.name.replace(/[\s\/\\]/g, '_');
                name = `CryptoNotes-${name}-export.json'`;
            var mime = 'json';
            var data = JSON.stringify(sr.export(), null, 4);

            download(data, name, mime);
        }
    }


}]);
