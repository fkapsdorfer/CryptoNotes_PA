/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file Password input with "insight" addon button.
 */

angular.module('CryptoNotes').directive('password', function () {

    return {
        replace: true,
        scope: {
            ngModel    : '=?',
            placeholder: '@?'
        },
        templateUrl: '/static/app/directives/password.html',

        link: function (scope, $wrapper) {

            var $input = $wrapper.find('input');
            var $addon = $wrapper.find('.show-pass');
            var $doc   = $(document);


            /**
             * Setup the focus + enter key functionality
             */
            var keydownHandler = (evt) => {
                //console.log('keydownHandler');
                if (evt.keyCode === 13) {
                    $doc.off('keydown', keydownHandler);
                    $doc.on('keyup', keyupHandler);
                    $input.attr('type', 'text');
                }
            };
            var keyupHandler = (evt) => {
                //console.log('keyupHandler');
                if (evt.keyCode === 13) {
                    $doc.off('keyup', keyupHandler);
                    $doc.on('keydown', keydownHandler);
                    $input.attr('type', 'password');
                }
            };
            var blurHandler = () => {
                //console.log('focusout');
                $addon.off('blur', blurHandler);
                $doc.off('keydown', keydownHandler);
            };

            $addon.on('focus', () => {
                //console.log('focus');
                $doc.on('keydown', keydownHandler);
                $addon.on('blur', blurHandler);
            });


            /**
             * Click, hold, and release functionality
             */
            var mouseupHandler = () => {
                $input.attr('type', 'password');
                $doc.off('mousedown touchstart', mouseupHandler);
            };

            $addon.on('mousedown touchstart', () => {
                $input.attr('type', 'text');
                $doc.on('mouseup touchend', mouseupHandler);
            });
        }
    }

});
