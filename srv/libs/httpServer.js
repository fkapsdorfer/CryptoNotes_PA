/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file defines and exports the HTTP server.
 */

'use strict';

var http 	= require('http');

var server = http.createServer();


module.exports = server;
