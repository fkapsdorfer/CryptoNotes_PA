/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file defines and exports the HTTPS server.
 */

'use strict';

var CONFIG  = require('../libs/config');
var https 	= require('https');
var fs      = require('fs');

var server = https.createServer({
    key : fs.readFileSync(CONFIG.HTTPS.KEY_PATH),
    cert: fs.readFileSync(CONFIG.HTTPS.CERT_PATH)
});


module.exports = server;
