/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file loads and exports the configuration.
 */

'use strict'

var fs = require('fs');


var configStr   = fs.readFileSync('srv/config.json', { flag: 'r' });
var CONFIG      = JSON.parse(configStr.toString());


module.exports = CONFIG;
