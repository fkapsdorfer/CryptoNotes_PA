/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This module lists (returns array) if local network addresses.
 */

'use strict'

var os = require('os');


module.exports = function getLocalIPs () {
    var addrs   = [];
    var ifaces  = os.networkInterfaces();

    for (var key in ifaces) {
        if (ifaces.hasOwnProperty(key)) {
            var iface = ifaces[key];

            for (var key in iface) {
                if (iface.hasOwnProperty(key)) {
                    addrs.push(iface[key].address);
                }
            }
        }
    }

    return addrs;
}
