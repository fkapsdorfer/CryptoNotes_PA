#!/usr/bin/env node
/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This script will read static dir and output json string for serviceWorker cache.
 */


 switch (process.argv[2]) {
     case '--object':
        actionPrintAsObject();
        break;
    case '--array':
        actionPrintAsArray();
        break;
    default:
        actionHelp();
 }



function actionPrintAsObject () {
    var fs = require('fs');

    // typeof null => file
    // typeof {} => dir
    var data = {
        static: {}
    };

    readDir('static');
    //console.log('');
    console.log(JSON.stringify(data, null, 4));


    function readDir (dirPath) {
        var files = fs.readdirSync(dirPath);
        files.forEach((file) => {
            var filePath = `${dirPath}/${file}`;
            var stat = fs.statSync(filePath);

            if (stat.isDirectory()) {
                console.log(`Processing dir: ${filePath}`);
                processDir(filePath);
                readDir(filePath);
            } else {
                processFile(filePath);
            }
        });
    }

    function processDir (dirPath) {
        //console.log(`Processing dir: ${dirPath}`);

        var obj = data;
        var pathComponents = dirPath.split('/');
        var len = pathComponents.length;

        for (var i = 0; i < len; i++) {
            var file = pathComponents[i];

            if (i === len-1) {
                // file is self (dirPath)
                obj[file] = {};
            } else {
                // file is some parent
                obj = obj[file];
            }
        }
    }

    function processFile (filePath) {
        //console.log(`Processing file: ${filePath}`);

        var obj = data;
        var pathComponents = filePath.split('/');
        var len = pathComponents.length;

        for (var i = 0; i < len; i++) {
            var file = pathComponents[i];

            if (i === len-1) {
                // file is self (filePath)
                obj[file] = null;
            } else {
                // file is some parent
                obj = obj[file];
            }
        }
    }
}

function actionPrintAsArray () {
    var fs = require('fs');
    var data = [];

    readDir('static');
    console.log(JSON.stringify(data, null, 4));


    function readDir (dirPath) {
        var files = fs.readdirSync(dirPath);
        files.forEach((file) => {
            var filePath = `${dirPath}/${file}`;
            var stat = fs.statSync(filePath);

            if (stat.isDirectory()) {
                readDir(filePath);
            } else {
                data.push(filePath);
            }
        });
    }
}

function actionHelp () {
    console.log('--help    Display this help');
    console.log('--array   Output as JSON object');
    console.log('--object  Output as JSON array');
}
