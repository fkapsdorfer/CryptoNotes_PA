/**
 * @author Filip Kapsdorfer
 * @copyright Filip Kapsdorfer 2016
 * @license MIT
 * @file This file defines and exports the Express application.
 */

'use strict'

var CONFIG      = require('./libs/config');
var fs          = require('fs');
var express     = require('express');
var bodyParser  = require('body-parser');
var morgan      = require('morgan');
var eapp        = express();


eapp.use(morgan('dev'));
eapp.use(bodyParser.json());
eapp.use(bodyParser.urlencoded({ extended: false }));

eapp.use('/static'          , express.static(process.cwd() + '/static'));
eapp.use('/robots.txt'      , (req, res) => res.sendFile(process.cwd() + '/static/app/robots.txt'));
eapp.use('/sw.js'           , (req, res) => res.sendFile(process.cwd() + '/static/app/sw.js'));
eapp.use('/'                , (req, res) => res.sendFile(process.cwd() + '/static/app/index.html'));

// Error middleware
eapp.use((err, req, res, next) => {
    // log ...
    console.error(err);
    res.status(500).send({ name: 'UnhandeledError', message: 'Something went wrong' })
});


module.exports = eapp;
